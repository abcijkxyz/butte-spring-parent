package com.boot.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

@RestController
@Tag(name = "Rest接口")
public class RestWeb {

    @Operation(summary = "Get接口")
    @GetMapping("rest/get/{id}")
    public String restGet(@PathVariable Integer id) {
        return "OK:"+id;
    }

    @Operation(summary = "Post接口")
    @PostMapping("/rest/post")
    public String restPost(@RequestBody ParamBO param){
        return "OK:"+param.getName();
    }

    @Operation(summary = "Put接口")
    @PutMapping("/rest/put")
    public String restPut(@RequestBody ParamBO param){
        return "OK:"+param.getId();
    }

    @Operation(summary = "Delete接口")
    @DeleteMapping("/rest/delete/{id}")
    public String restDelete(@PathVariable Integer id){
        return "OK:"+id;
    }
}