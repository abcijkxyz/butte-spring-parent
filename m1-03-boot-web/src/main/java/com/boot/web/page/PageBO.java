package com.boot.web.page;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-12 16:41
 */
public class PageBO {

    private Integer key ;

    private String value ;

    public PageBO(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
