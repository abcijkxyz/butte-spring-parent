package com.boot.senior.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-08 18:27
 */
@Service
public class AsyncService {

    private static final Logger log = LoggerFactory.getLogger(AsyncService.class);

    /*
     * [schedule-pool-1] c.boot.senior.schedule.ScheduleService   : async-job-02-end...
     * [cTaskExecutor-1] c.boot.senior.schedule.ScheduleService   : async-job-01-end...
     *
     */

    @Async
    public void asyncJob (){
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("async-job-01-end...");
    }

    @Async("asyncPool")
    public void asyncJobPool (){
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("async-job-02-end...");
    }
}
