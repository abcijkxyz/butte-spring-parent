package com.boot.senior.schedule;

import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-08 14:26
 */
@RestController
public class AsyncWeb {

    @Resource
    private AsyncService asyncService ;

    @GetMapping("/async")
    public String async (){
        asyncService.asyncJob();
        asyncService.asyncJobPool();
        return "OK-async" ;
    }
}
