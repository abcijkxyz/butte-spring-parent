package com.boot.senior.aop;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-04 14:51
 */
@RestController
public class AopWeb {

    @GetMapping("/aop/web1")
    @DefAop(modelDesc="Aop测试1",otherInfo="其他信息")
    public String aopWeb1(){
        return "aop-web" ;
    }
}
