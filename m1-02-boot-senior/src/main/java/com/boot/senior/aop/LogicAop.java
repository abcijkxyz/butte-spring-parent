package com.boot.senior.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.lang.reflect.Method;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-04 16:08
 */
@Component
@Aspect
public class LogicAop {

    private static final Logger logger = LoggerFactory.getLogger(LogicAop.class) ;
    /**
     * 切入点
     */
    @Pointcut("@annotation(com.boot.senior.aop.DefAop)")
    public void defAopPointCut() {

    }

    /**
     * 环绕切入
     */
    @Around("defAopPointCut()")
    public Object around (ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object result = null ;
        try{
            // 执行方法
            result = proceedingJoinPoint.proceed();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            // 处理逻辑
            buildLogicAop(proceedingJoinPoint) ;
        }
        return result ;
    }

    /**
     * 构建处理逻辑
     */
    private void buildLogicAop (ProceedingJoinPoint point){
        // 获取方法
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method reqMethod = signature.getMethod();

        // 获取注解
        DefAop defAop = reqMethod.getAnnotation(DefAop.class);
        String modelDesc = defAop.modelDesc() ;
        String otherInfo = defAop.otherInfo();
        logger.info("DefAop-modelDesc：{}",modelDesc);
        logger.info("DefAop-otherInfo：{}",otherInfo);
    }
}
