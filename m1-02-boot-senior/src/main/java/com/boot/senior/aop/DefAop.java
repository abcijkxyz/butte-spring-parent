package com.boot.senior.aop;

import java.lang.annotation.*;

/**
 * 自定义注解
 * @author 公众号:知了一笑
 * @since 2023-07-04 15:56
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface DefAop {
    /**
     * 模块描述
     */
    String modelDesc();

    /**
     * 其他信息
     */
    String otherInfo();
}
