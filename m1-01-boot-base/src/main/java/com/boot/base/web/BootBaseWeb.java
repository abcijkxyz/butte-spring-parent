package com.boot.base.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-02 11:42
 */
@RestController
public class BootBaseWeb {

    /**
     * 测试：localhost:8082/boot/base/99
     * @param id 请求参数
     * @return java.util.Map<java.lang.String,java.lang.String>
     * @since 2023-07-02 12:06
     */
    @GetMapping("/boot/base/{id}")
    public Map<String,String> getInfo (@PathVariable String id){
        if (id.isBlank() || "0".equals(id)){
            throw new RuntimeException("参数ID错误");
        }
        var dataMap = new HashMap<String,String>();
        dataMap.put("id",id);
        dataMap.put("boot","base");
        return dataMap ;
    }
}
