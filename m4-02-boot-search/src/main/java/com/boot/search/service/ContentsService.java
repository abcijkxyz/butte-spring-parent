package com.boot.search.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.boot.search.entity.Contents;
import com.boot.search.mapper.ContentsMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-28 13:29
 */
@Service
public class ContentsService {

    @Resource
    private ContentsMapper contentsMapper ;

    public List<Contents> queryAll (){
        return contentsMapper.selectList(new QueryWrapper<>());
    }

}
