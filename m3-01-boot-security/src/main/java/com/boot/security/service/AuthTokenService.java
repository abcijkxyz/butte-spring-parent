package com.boot.security.service;

import jakarta.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import java.util.concurrent.TimeUnit;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-23 11:38
 */
@Service
public class AuthTokenService {

    private static final Logger log = LoggerFactory.getLogger(AuthTokenService.class);

    @Resource
    private RedisTemplate<String,Object> redisTemplate ;

    public String createToken (User user){
        String userName = user.getUsername();
        String token = DigestUtils.md5DigestAsHex(userName.getBytes());
        log.info("user-name:{},create-token:{}",userName,token);
        redisTemplate.opsForValue().set(token,user,10, TimeUnit.MINUTES);
        return token ;
    }

    public Object getToken (String token){
        return redisTemplate.opsForValue().get(token);
    }

    public Boolean deleteToken (String token){
        return redisTemplate.delete(token);
    }
}
