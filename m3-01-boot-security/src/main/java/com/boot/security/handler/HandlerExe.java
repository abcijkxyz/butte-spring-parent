package com.boot.security.handler;

import com.boot.security.entity.Rep;
import com.boot.security.exception.AuthException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-02 15:06
 */
@RestControllerAdvice
public class HandlerExe {

    @ExceptionHandler(value = AuthException.class)
    public Rep<?> handlerAuthException (AuthException e){
        return new Rep<>(HttpStatus.FORBIDDEN.value(),e.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    public Rep<?> handlerException (HttpServletRequest request, Exception e){
        return new Rep<>(HttpStatus.INTERNAL_SERVER_ERROR.value(),e.getMessage());
    }
}
