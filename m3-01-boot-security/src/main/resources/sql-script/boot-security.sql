CREATE TABLE `tb_user_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_name` varchar(30) NOT NULL COMMENT '用户名称',
  `pass_word` varchar(200) DEFAULT NULL COMMENT '密码',
  `user_role` varchar(50) DEFAULT NULL COMMENT '用户角色',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户基础表';

INSERT INTO `boot-security`.`tb_user_base` (`id`, `user_name`, `pass_word`, `user_role`, `create_time`) VALUES (1, 'admin', '$2a$10$6SRsvF07DYMxs7m8YXiaBuVZdNo/PLgXd/ThMjGMIqYmXRvQlNbjS', 'ROLE_Admin', '2023-07-22 06:58:39');
INSERT INTO `boot-security`.`tb_user_base` (`id`, `user_name`, `pass_word`, `user_role`, `create_time`) VALUES (2, 'user', '$2a$10$uxMvq5PECMZ.k5Dom7xzm.rOPFoJ8fy4m2xtGLCs3QMcdn3Bts2vG', 'ROLE_User', '2023-07-22 07:02:10');