package com.boot.redis;

import com.boot.redis.entity.Article;
import com.boot.redis.mapper.ArticleMapper;
import jakarta.annotation.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-21 17:34
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MybatisTest {

    @Resource
    private ArticleMapper articleMapper ;

    @Test
    public void testInsert (){
        List<Article> articles = Arrays.asList(
                new Article("Java","Java编程语言",new Date()),
                new Article("SQL","数据库编程语言",new Date()));
        articles.forEach(articleMapper::insert);
    }

    @Test
    public void testQuery (){
        // id=1 缓存读失败，读数据库，写入缓存
        Article first = articleMapper.selectById(1) ;
        // id=1 缓存读成功
        Article second = articleMapper.selectById(1) ;
    }

    @Test
    public void testUpdate (){
        // id=2 缓存读失败，读数据库，写入缓存
        Article first = articleMapper.selectById(2) ;
        if (first != null){
            first.setCreateTime(new Date());
            // id=2 更新，缓存清空
            articleMapper.updateById(first) ;
            // id=2 缓存读失败，读数据库，写入缓存
            Article second = articleMapper.selectById(2) ;
        }
    }

    @Test
    public void testDelete (){
        // id=2 缓存读成功
        Article first = articleMapper.selectById(2) ;
        if (first != null){
            // id=2 删除，缓存清空
            articleMapper.deleteById(2) ;
        }
    }
}
