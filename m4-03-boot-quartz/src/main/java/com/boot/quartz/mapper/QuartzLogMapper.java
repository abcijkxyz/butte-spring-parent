package com.boot.quartz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.quartz.entity.QuartzLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface QuartzLogMapper extends BaseMapper<QuartzLog> {

}