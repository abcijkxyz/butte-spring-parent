package com.boot.file.entity;

import com.alibaba.excel.annotation.ExcelProperty;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-15 09:57
 */
public class DataVO {

    @ExcelProperty("编号")
    private Integer id ;
    @ExcelProperty("名称")
    private String name ;
    @ExcelProperty("手机号")
    private String phone ;
    @ExcelProperty("城市")
    private String cityName ;
    @ExcelProperty("日期")
    private Date date ;

    public static List<DataVO> getSheet1List (){
        return Arrays.asList(
                new DataVO(1,"张三","18952346978","北京",new Date()),
                new DataVO(2,"李四","18952346977","上海",new Date()),
                new DataVO(3,"王五","18952346976","广州",new Date())
                );
    }

    public static List<DataVO> getSheet2List (){
        return Arrays.asList(
                new DataVO(4,"赵六","18952346975","杭州",new Date()),
                new DataVO(5,"孙七","18952346974","深圳",new Date()),
                new DataVO(6,"周八","18952346973","乡村",new Date())
        );
    }

    public DataVO() {
    }

    public DataVO(Integer id, String name, String phone, String cityName, Date date) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.cityName = cityName;
        this.date = date;
    }

    @Override
    public String toString() {
        return "DataVO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", cityName='" + cityName + '\'' +
                ", date=" + date +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}