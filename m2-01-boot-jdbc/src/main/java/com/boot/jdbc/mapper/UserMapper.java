package com.boot.jdbc.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.boot.jdbc.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.jdbc.entity.UserModel;
import org.apache.ibatis.annotations.Param;

/**
 * 用户基础信息 Mapper 接口
 * @author 公众号:知了一笑
 * @since 2023-07-16 19:31
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 自定义分页
     */
    IPage<UserModel> queryUserPage(@Param("page") IPage<User> page);
}
