package com.boot.jdbc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author 公众号:知了一笑
 * @since 2023-07-16 13:39
 */
@Data
@ToString
@TableName("tb_user_extd")
public class UserExtd implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 城市名称
     */
    private String cityName;

    /**
     * 学校名称
     */
    private String school;
}
