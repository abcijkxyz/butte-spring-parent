package com.boot.jdbc.dao;

import com.boot.jdbc.entity.UserExtd;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户扩展信息 服务类
 * @author 公众号:知了一笑
 * @since 2023-07-16 19:42
 */
public interface UserExtdDao extends IService<UserExtd> {

}
