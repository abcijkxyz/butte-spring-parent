package com.boot.jdbc.service;

import com.boot.jdbc.entity.User;
import jakarta.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Jdbc服务类
 * @author 公众号:知了一笑
 * @since 2023-07-16 10:58
 */
@Service
public class JdbcService {

    private static final Logger logger = LoggerFactory.getLogger(JdbcService.class);

    @Resource
    private JdbcTemplate jdbcTemplate ;

    public int addData (User user){
        return jdbcTemplate.update(
                "INSERT INTO `tb_user` (`user_name`, `email`, `phone`, `create_time`, `update_time`) " +
                    "VALUES (?, ?, ?, ?, ?)",
                user.getUserName(),user.getEmail(),user.getPhone(),user.getCreateTime(),user.getUpdateTime());
    }

    public List<User> queryAll (){
        return jdbcTemplate.query("SELECT * FROM tb_user WHERE state=1",new BeanPropertyRowMapper<>(User.class));
    }

    public int updateName (Integer id,String name){
        return jdbcTemplate.update("UPDATE `tb_user` SET `user_name` = ? WHERE `id` = ?",name,id);
    }

    public int deleteId (Integer id){
        return jdbcTemplate.update("DELETE FROM `tb_user` WHERE `id` = ?",id);
    }
}
